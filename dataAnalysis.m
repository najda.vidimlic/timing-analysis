% Plot instance trace graph (stem) for response time and wakeup latency
figure
x =linspace(min,max,tot);
stem(x,ResponseTime);

hold on
stem(x,WakeupLatency);

xlim([xmin,xmax])

set(gca,'FontSize',12)
breakyaxis([ymin ymax]); %import breakyaxis.m

% Plot instance trace graph for response time
figure
plot(x,ResponseTime);
xlim([xmin,xmax])
set(gca,'FontSize',12)
xlabel('Instance')
ylabel('Response time')
grid on;

% Histogram for response time 
figure
max_Resp=max(ResponseTime);
min_Resp=min(ResponseTime);
num_Resp = max_Resp - min_Resp;


edges = linspace(min_Resp,max_Resp,num_Resp);
histogram(ResponseTime, 'BinEdges',edges);
xlim([xmin,xmax])
set(gca,'FontSize',12)
xlabel('Response time (μs)')
ylabel('Number of instances')

% Historgram showing nterference effects on response time
max_RespIdle=max(ResponseTimeIdle);
min_RespIdle=min(ResponseTimeIdle);
num_RespIdle = max_RespIdle - min_RespIdle;

edgesIdle = linspace(min_RespIdle,max_RespIdle,num_RespIdle);
histogram(ResponseTimeIdle, 'BinEdges',edgesIdle);
hold on 

max_RespKsExe=max(ResponseTimeKsExe);
min_RespKsExe=min(ResponseTimeKsExe);
num_RespKsExe = max_RespKsExe - min_RespKsExe;

edgesKsExe = linspace(min_RespKsExe,max_RespKsExe,num_RespKsExe);
histogram(ResponseTimeKsExe, 'BinEdges',edgesKsExe);

hold on

max_RespKsSirqExe=max(ResponseTimeKsSirqExe);
min_RespKsSirqExe=min(ResponseTimeKsSirqExe);
num_RespKsSirqExe = max_RespKsSirqExe - min_RespKsSirqExe;

edgesKsSirqExe = linspace(min_RespKsSirqExe,max_RespKsSirqExe,num_RespKsSirqExe);
histogram(ResponseTimeKsSirqExe, 'BinEdges',edgesKsSirqExe);

hold on

max_RespKsWake=max(ResponseTimeKsWake);
min_RespKsWake=min(ResponseTimeKsWake);
num_RespKsWake = max_RespKsWake - min_RespKsWake;

edgesKsWake = linspace(min_RespKsWake,max_RespKsWake,num_RespKsWake);
histogram(ResponseTimeKsWake, 'BinEdges',edgesKsWake);

hold on

max_RespKsSwap=max(ResponseTimeKsSwap);
min_RespKsSwap=min(ResponseTimeKsSwap);
num_RespKsSwap = max_RespKsSwap - min_RespKsSwap;

edgesKsSwap = linspace(min_RespKsSwap,max_RespKsSwap,num_RespKsSwap);
histogram(ResponseTimeKsSwap, 'BinEdges',edgesKsSwap);

set(gca,'FontSize',12)
xlabel('Response time (μs)')
ylabel('Number of instances')
xlim([xmin,xmax])
legend('show')